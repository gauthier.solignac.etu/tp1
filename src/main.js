let data = [
    {
        name: 'Regina',
        base: 'tomate',
        price_small: 6.5,
        price_large: 9.95,
        image: 'https://images.unsplash.com/photo-1532246420286-127bcd803104?fit=crop&w=500&h=300'
    },
    {
        name: 'Napolitaine',
        base: 'tomate',
        price_small: 6.5,
        price_large: 8.95,
        image: 'https://images.unsplash.com/photo-1562707666-0ef112b353e0?&fit=crop&w=500&h=300'
    },
    {
        name: 'Spicy',
        base: 'crème',
        price_small: 5.5,
        price_large: 8,
        image: 'https://images.unsplash.com/photo-1458642849426-cfb724f15ef7?fit=crop&w=500&h=300',
    }
];
data.sort(function(a, b){
    if(a.name < b.name) { return -1; }
    if(a.name > b.name) { return 1; }
    return 0;
});


data.sort(function(a, b) {
    if(a.price_small == b.price_small){
        return a.price_large - b.price_large;
    }
    return a.price_small - b.price_small;
});

function baseTomate(element) {
    const {base} = element
    if(base == 'tomate'){
        return element;
    }
}

//data = data.filter(baseTomate);

function prixPlusPetitQue6(element){
    const {price_small} = element;
    if(price_small < 6){
        return element;
    }
}

//data = data.filter(prixPlusPetitQue6)

function char_count(str, letter) 
{
 let letter_Count = 0;
 for (let position = 0; position < str.length; position++) 
 {
    if (str.charAt(position) == letter) 
      {
      letter_Count += 1;
      }
  }
  return letter_Count;
}

function bahenfaitcacontientidoncondoitletrier(element){
    const {name} = element;
    if(char_count(name, "i") >= 2){
        return element;
    }
}

//data = data.filter(bahenfaitcacontientidoncondoitletrier);

const name = 'Regina';
//const url ='images/' + name.toLowerCase() + ".jpg";
for(let i = 0; i < data.length; i++){
    const { name, base, price_small, price_large, image } = data[i];
    let html = `<article class="pizzaThumbnail">
    <a href="${image}">
    <img src="${image}"/>
    <section>
    <h4>${name}</h4>
    <ul>
    <li>Prix du petit format:${price_small}</li>
    <li>Prix grand format:${price_large}</li>
    </ul>
    </section>
    </a>
    </article>`;
    document.querySelector('.pageContent').innerHTML += html; 
}


const character = { firstName: 'Skyler', lastName: 'White' };

const { firstName, lastName } = character;

function kill(character) {
	console.log( `${character.firstName} ${character.lastName} is dead :'(` );
}

kill(character);
