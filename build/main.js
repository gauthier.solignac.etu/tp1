"use strict";

var data = [{
  name: 'Regina',
  base: 'tomate',
  price_small: 6.5,
  price_large: 9.95,
  image: 'https://images.unsplash.com/photo-1532246420286-127bcd803104?fit=crop&w=500&h=300'
}, {
  name: 'Napolitaine',
  base: 'tomate',
  price_small: 6.5,
  price_large: 8.95,
  image: 'https://images.unsplash.com/photo-1562707666-0ef112b353e0?&fit=crop&w=500&h=300'
}, {
  name: 'Spicy',
  base: 'crème',
  price_small: 5.5,
  price_large: 8,
  image: 'https://images.unsplash.com/photo-1458642849426-cfb724f15ef7?fit=crop&w=500&h=300'
}];
data.sort(function (a, b) {
  if (a.name < b.name) {
    return -1;
  }

  if (a.name > b.name) {
    return 1;
  }

  return 0;
});
data.sort(function (a, b) {
  if (a.price_small == b.price_small) {
    return a.price_large - b.price_large;
  }

  return a.price_small - b.price_small;
});

function baseTomate(element) {
  var base = element.base;

  if (base == 'tomate') {
    return element;
  }
} //data = data.filter(baseTomate);


function prixPlusPetitQue6(element) {
  var price_small = element.price_small;

  if (price_small < 6) {
    return element;
  }
} //data = data.filter(prixPlusPetitQue6)


function char_count(str, letter) {
  var letter_Count = 0;

  for (var position = 0; position < str.length; position++) {
    if (str.charAt(position) == letter) {
      letter_Count += 1;
    }
  }

  return letter_Count;
}

function bahenfaitcacontientidoncondoitletrier(element) {
  var name = element.name;

  if (char_count(name, "i") >= 2) {
    return element;
  }
} //data = data.filter(bahenfaitcacontientidoncondoitletrier);


var name = 'Regina'; //const url ='images/' + name.toLowerCase() + ".jpg";

for (var i = 0; i < data.length; i++) {
  var _data$i = data[i],
      _name = _data$i.name,
      base = _data$i.base,
      price_small = _data$i.price_small,
      price_large = _data$i.price_large,
      image = _data$i.image;
  var html = "<article class=\"pizzaThumbnail\">\n    <a href=\"".concat(image, "\">\n    <img src=\"").concat(image, "\"/>\n    <section>\n    <h4>").concat(_name, "</h4>\n    <ul>\n    <li>Prix du petit format:").concat(price_small, "</li>\n    <li>Prix grand format:").concat(price_large, "</li>\n    </ul>\n    </section>\n    </a>\n    </article>");
  document.querySelector('.pageContent').innerHTML += html;
}

var character = {
  firstName: 'Skyler',
  lastName: 'White'
};
var firstName = character.firstName,
    lastName = character.lastName;

function kill(character) {
  console.log("".concat(character.firstName, " ").concat(character.lastName, " is dead :'("));
}

kill(character);
//# sourceMappingURL=main.js.map